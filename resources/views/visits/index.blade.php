@extends('layouts.admin')

@section('header_scripts')
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>

<script 
    src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
    crossorigin="anonymous"></script> 
@endsection

@section('content')
@include('includes.flash_msgs')
<div class="main-card mb-3 card mt-4">
	<div class="card-header-tab card-header-tab-animation card-header">
		<div class="card-header-title font-size-lg text-capitalize font-weight-normal">
			<i class="fas fa-users"></i>Моите посещения
		</div>
	</div>
	
	<div class="card-body">
	    <table style="width: 100%;" id="schoolVisitsTable" class="table table-hover table-striped table-bordered table-responsive">
                    <thead>
                        <tr>
                            <th>№</th>
                            @if(!$isTeacher)
                            <th>Учител</th>
                            <th>Телефон</th>
                            <th>Ел. поща</th>
                            <th>Училище</th>
                            <th>Населено място</th>
                            @endif
                            
                            @if(!$isProfessional)
                            <th>Ролеви модел</th>
                            <th>Телефон</th>
                            <th>Ел. поща</th>
                            <th>Удобно време за посещение</th>
                            @endif
                            <th>Статус</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($schoolVisits as $schoolVisit)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            @if(!$isTeacher)
                                <td>
                                    @if($schoolVisit->teacher && $schoolVisit->teacher->user)              
                                        {{$schoolVisit->teacher->user->fullNames}}
                                    @endif
                                </td>
                                <td>
                                    @if($schoolVisit->teacher && $schoolVisit->teacher->user)              
                                        {{$schoolVisit->teacher->user->phone}}
                                    @endif
                                </td>
                                <td>
                                    @if($schoolVisit->teacher && $schoolVisit->teacher->user)              
                                        {{$schoolVisit->teacher->user->email}}
                                    @endif
                                </td>
                                <td>
                                    @if($schoolVisit->teacher && $schoolVisit->teacher->school)              
                                        {{$schoolVisit->teacher->school->name}}
                                    @endif
                                </td>
                                <td>
                                    @if($schoolVisit->teacher && $schoolVisit->teacher->school->city)              
                                        {{$schoolVisit->teacher->school->city->name}}
                                    @endif
                                </td>
                            @endif
                            
                            @if(!$isProfessional)
                                <td>
                                    @if($schoolVisit->professional && $schoolVisit->professional->user)
                                        {{$schoolVisit->professional->user->fullNames}}
                                    @endif
                                </td>
                                <td>
                                    @if($schoolVisit->professional && $schoolVisit->professional->user)
                                        {{$schoolVisit->professional->user->phone}}
                                    @endif
                                </td>
                                <td>
                                    @if($schoolVisit->professional && $schoolVisit->professional->user)
                                        {{$schoolVisit->professional->user->email}}
                                    @endif
                                </td>
                                <td>
                                    @if($schoolVisit->professional)
                                        {{$schoolVisit->professional->available_start_date}} - {{$schoolVisit->professional->available_end_date}}
                                    @endif
                                </td>
                            @endif
                            <td>
                                @if($schoolVisit->requestStatus)
                                    {{$schoolVisit->requestStatus->name}}
                                @endif
                            </td>
                            <td></td>
                        </tr>
                        @empty
                            <tr>
                                <td></td>
                                <td colspan="11">Все още нямате потвърдени посещения за този срок!</td>
                            </tr>
                    @endforelse
                    </tbody>
        </table>
	</div>
</div>
@endsection

@section('footer_scripts')
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" />
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#schoolVisitsTable').DataTable({
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.21/i18n/Bulgarian.json"
            }
        });
    });
</script>