@extends('layouts.admin')

@section('header_scripts')
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
@endsection

@section('content')
@include('includes.flash_msgs')
<div class="main-card mb-3 card mt-4">
    <div class="card-header-tab card-header-tab-animation card-header">
        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
            <i class="fas fa-users"></i>Ролеви модели
        </div>
    </div>
</div>

<div class="card-body">
    <table style="width: 100%;" id="professionalsTable" class="table table-hover table-striped table-bordered table-responsive">
                    <thead>
                        <tr>
                            <th>№</th>
                            <th>Имена</th>
                            <th>Компания</th>
                            <th>Позиция</th>
                            
                            <th>Населено място</th>
                            <th>Сфера</th>
                            <th>Вид посещение</th>
                            <th>Предпочитани класове</th>
                            <th>Възможни дати</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($professionals as $professional)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$professional->user->first_name}} {{$professional->user->last_name}}</td>
                                <td>
                                    {{$professional->company->name}}
                                </td>
                                <td>
                                    {{$professional->position}}
                                </td>
                                
                                <td>
                                    TODO
                                </td>
                                <td>{{$professional->roleModelProfession->name}}</td>
                                <td>{{$professional->is_only_online ? 'онлайн' : 'на живо'}}</td>
                                <td>
                                    @switch($professional->desired_classes)
                		                @case('1-4') 1-4 клас @break
                		                @case('5-7') 5-7 клас @break
                		                @case('8-12') 8-12 клас @break
                		                @case('extra classes') занимална с часове по интереси @break
                		                @case('all') без предпочитания @break
                		            @endswitch
                                </td>
                                <td>{{$professional->available_start_date}} - {{$professional->available_end_date}}</td>
                                <td>
                                    <a href='{{url("/professionals/show/$professional->id")}}' target="_blank" class="btn btn-info">Детайли</a>
                                </td>
                                <td>
                                    <form action='{{url("/professionals/select")}}' method="post">
                                        @csrf
                                        <input type="hidden" name="professional_id" value="{{$professional->id}}"/>
                                        <input type="submit" name="save" class="btn btn-primary arrange-visit-btn" value="Заяви посещение"/>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td></td>
                                <td colspan="10">Няма потребители с роля ролеви модел или мениджър ролеви модели!</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
</div>

@endsection

@section('footer_scripts')
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" />
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#professionalsTable').DataTable({
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.21/i18n/Bulgarian.json"
            }
        });
        
        $('.arrange-visit-btn').on('click',function(e){
            let answer = confirm('Сигурни ли сте, че искате да включите този ролеви модел за посещение във вашат класна стая?');

            if(answer){
             $(this).parents("form").submit();
            }
            else{
             e.preventDefault();      
            }
        });
        
    });
</script>
@endsection