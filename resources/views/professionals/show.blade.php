@extends('layouts.admin')

@section('header_scripts')
<script
src="https://code.jquery.com/jquery-3.5.1.min.js"
integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
crossorigin="anonymous"></script>
@endsection

@section('content')
<div class="main-card mb-3 card mt-4">
	<div class="card-header-tab card-header-tab-animation card-header">
		<div class="card-header-title font-size-lg text-capitalize font-weight-normal">
			<i class="fas fa-user-astronaut"></i> &nbsp; Ролеви модел - {{$professional->user->fullNames}}
		</div>
	</div>
	
	<div class="card-body">
		<div class="tab-content">
		    <div class="row">
		        <div class="col-md-3"><strong>Име:</strong></div>
		        <div class="col-md-8">{{$professional->user->fullNames}}</div>
		    </div>
		    <br>
		    <div class="row">
		        <div class="col-md-3"><strong>Организация:</strong></div>
		        <div class="col-md-8">{{$professional->company->name}}</div>
		    </div>
		    <br>
		    <div class="row">
		        <div class="col-md-3"><strong>Позиция:</strong></div>
		        <div class="col-md-8">{{$professional->position}}</div>
		    </div>
		    <br>
		    <div class="row">
		        <div class="col-md-3"><strong>Сфера:</strong></div>
		        <div class="col-md-8">{{$professional->roleModelProfession->name}}</div>
		    </div>
		    <br>
		    <div class="row">
		        <div class="col-md-3"><strong>На живо или онлайн:</strong></div>
		        <div class="col-md-8">{{$professional->is_only_online ? 'онлайн' : 'на живо'}}</div>
		    </div>
		    <br>
		    <div class="row">
		        <div class="col-md-3"><strong>Възможни дати за посещение :</strong></div>
		        <div class="col-md-8">{{$professional->available_start_date}} - {{$professional->available_end_date}}</div>
		    </div>
		    <br>
		    <div class="row">
		        <div class="col-md-3"><strong>Предпочитан начин на комуникация:</strong></div>
		        <div class="col-md-8">
		            @switch($professional->communication_type)
		                @case('phone') телефон @break
		                @case('email') имейл @break
		                @default без предпочитания @break
		            @endswitch
		        </div>
		    </div>
		    <br>
		    <div class="row">
		        <div class="col-md-3"><strong>Предпочитани класове за посещение:</strong></div>
		        <div class="col-md-8">
		            @switch($professional->desired_classes)
		                @case('1-4') 1-4 клас @break
		                @case('5-7') 5-7 клас @break
		                @case('8-12') 8-12 клас @break
		                @case('extra classes') занимална с часове по интереси @break
		                @case('all') без предпочитания @break
		            @endswitch
		        </div>
		    </div>
		    <br>
		    <div class="row">
		        <div class="col-md-3"><strong>Нужно ли е оборудване:</strong></div>
		        <div class="col-md-8">{{$professional->is_equipment_required ? 'да' : 'не'}}</div>
		    </div>
		</div>
	</div>
</div>
@endsection