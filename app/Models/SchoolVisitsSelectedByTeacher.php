<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolVisitsSelectedByTeacher extends Model
{
    protected $fillable = [
        'professional_id', 'teacher_id', 'date', 'visit_status_id'
    ];

    protected $casts = [
        'date' => 'date',
        'teacher_id' => 'integer',
        'professional_id' => 'integer',
        'visit_status_id' => 'integer'
    ];

    public function teacher()
    {
    	return $this->belongsTo('App\Models\Teacher');
    }

    public function professional()
    {
    	return $this->belongsTo('App\Models\Professional');
    }
    
    public function requestStatus()
    {
        return $this->belongsTo('App\Models\RequestStatus', 'visit_status_id');
    }
    
    public static function fetchVisitsByAuthUser()
    {
        $schoolVisits = self::with(['teacher', 'professional'])->where('visit_status_id', '!=', config('consts.REQUEST_STATUS_ARCHIVED'));
        
        if (\Auth::user()->role_id === config('consts.ROLE_ID_TEACHER')){
            $schoolVisits = $schoolVisits->where('teacher_id', \Auth::user()->teacher->id);
        } elseif(\Auth::user()->role_id === config('consts.ROLE_ID_PROFESSIONAL')) {
            $schoolVisits = $schoolVisits->where('professional_id', \Auth::user()->professional->id);
        }
        
        return $schoolVisits->get();
    }
}