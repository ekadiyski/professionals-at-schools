<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Professional extends Model
{
    protected $fillable = [
        'user_id', 'company_id', 'position', 'role_model_profession_id', 'is_only_online',
        'cities', 'available_start_date', 'available_end_date', 'communication_type', 'desired_classes',
        'is_equipment_required'
    ];

    protected $casts = [
        'user_id' => 'integer',
        'company_id' => 'integer'
    ];

    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }

    public function company()
    {
    	return $this->belongsTo('App\Models\Company');
    }

    public function schoolVisits()
    {
        return $this->hasMany('App\Models\SchoolVisit');
    }
    
    public function roleModelProfession() {
        return $this->belongsTo('App\Models\RoleModelProfession');
    }
    
    public static function fetchAvailableRoleModels()
    {
        return self::where('company_id', 1005)->where('status', 'pending')->where('updated_at', '>', '2022-09-15')->get();
    }
}
