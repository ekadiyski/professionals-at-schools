<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserProfile;
use App\Models\Teacher;
use App\Models\School;
use App\Models\Subject;
use App\Models\User;
use App\Models\Company;
use App\Models\Professional;
use App\Models\RoleModelProfession;
use App\Models\City;
use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
    public function index()
    {
        $teachers = User::fetchTeachers();
        $professionals = User::fetchProfessionals();
        $admins = User::fetchAdmins();

        return view('user.index', [
            'teachers' => $teachers,
            'professionals' => $professionals,
            'admins' => $admins
        ]);
    }
    
    public function destroy(User $user, Request $request)
    {
        //TODO: check if there are active school visit requests
        if(Auth::id() != $user->id) {
            abort(403);
        }

        $validated = $request->validate([
            'confirm_delete' => 'required'
        ]);
        if($validated['confirm_delete'] != 'delete') {
            return redirect('/profile')->with('msg_delete', "Грешка при изтриване на профил! За да изтриете профила си въведете думата 'delete' в текстовото поле!");  
        }

        $user->delete();

        return redirect('/');
    }

    public function profile()
    {
    	$user = Auth::user();

    	$data = [];
    	if($user->role_id === config('consts.ROLE_ID_TEACHER')){
    		$data['teacher'] = Teacher::with('subjects')->where('user_id', $user->id)->first();
    		$data['schools'] = School::all();
    		$data['subjects'] = Subject::all();
    	} elseif ($user->role_id === config('consts.ROLE_ID_PROFESSIONAL')) {
            $data['professional'] = Professional::with('company')->where('user_id', $user->id)->first();
            $data['companies'] = Company::whereIn('id', [999, 1005])->get();
            $data['roleModelProfessions'] = RoleModelProfession::all();
            $data['cities'] = City::all();
        }
    	
    	return view('user.profile', [
    		'user' => $user,
    		'data' => $data
    	]);
    }

    public function profileStore(UpdateUserProfile $request)
    {
        $user = \Auth::user();
        $user->first_name = $request->first_name;
        $user->middle_name = $request->middle_name;
        $user->last_name = $request->last_name;
        $user->phone = $request->phone;
        $user->save();

        if($user->role_id === config('consts.ROLE_ID_TEACHER')){
            $teacher = Teacher::where('user_id', $user->id)->first();
            $teacher->school_id = $request->school_id;
            $teacher->save();

            $teacher->subjects()->sync($request->subject_ids);
        } elseif ($user->role_id === config('consts.ROLE_ID_PROFESSIONAL')) {
            $isOnlyOnline = 0;
            if(!empty($request->is_only_online)) {
                $isOnlyOnline = 1;
            }
                
            $professional = Professional::where('user_id', $user->id)->first();
            $professional->company_id = $request->company_id;
            $professional->position = $request->position;
            
            $professional->role_model_profession_id = $request->role_model_profession_id;
            $professional->is_only_online = $isOnlyOnline;
            $professional->cities = $request->cities;
            $professional->available_start_date = $request->available_start_date;
            $professional->available_end_date = $request->available_end_date;
            $professional->desired_classes = $request->desired_classes;
            $professional->communication_type = $request->communication_type;
            $professional->is_equipment_required = $request->is_equipment_required;
            $professional->save();
        }

        return redirect('/profile')->with('msg_update', 'Профилът беше успешно обновен!');
    }
    
    public function showProfessional() 
    {
        $professionalId = request()->id;
        $professional = Professional::find($professionalId);
        
        return view('professionals.show', [
            'professional' => $professional    
        ]);
    }
    
    public function indexProfessionals() 
    {
        $professionals = Professional::fetchAvailableRoleModels();
        
        return view('professionals.index', [
            'professionals' => $professionals
        ]);
    }
}
