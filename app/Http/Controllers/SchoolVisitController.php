<?php

namespace App\Http\Controllers;

use App\Models\SchoolVisitsSelectedByTeacher;
use App\Models\Professional;
use App\Mail\SchoolVisitRequestApproved;
use App\Mail\SchoolVisitRequestRoleModel;
use App\Services\Util;
use Illuminate\Http\Request;

class SchoolVisitController extends Controller
{
    public function index()
    {
        $schoolVisits = SchoolVisitsSelectedByTeacher::fetchVisitsByAuthUser();
        if(\Auth::check()){
            $isTeacher = \Auth::user()->isTeacher();
            $isProfessional = \Auth::user()->isProfessional();
        } else {
            $isTeacher = false;
            $isProfessional = false;
        }
        
        return view('visits.index', [
            'schoolVisits' => $schoolVisits,
            'isTeacher' => $isTeacher,
            'isProfessional' => $isProfessional,
        ]);
    }
    
    public function professionalsSelect(Request $request)
    {
        try{
            \DB::beginTransaction();
            $schoolVisitsSelectedByTeacher = new SchoolVisitsSelectedByTeacher();
            $schoolVisitsSelectedByTeacher->professional_id = $request['professional_id'];
            $schoolVisitsSelectedByTeacher->teacher_id = \Auth::user()->teacher->id;
            $schoolVisitsSelectedByTeacher->save();
            
            $professional = Professional::find($request['professional_id']);
            $professional->status='arranged';
            $professional->save();
            
            \DB::commit();
            
            //email to teacher
            \Mail::to(\Auth::user()->email)->queue(new SchoolVisitRequestApproved($schoolVisitsSelectedByTeacher));
                
            //email to role model
            
            if($professional && $professional->user)
            {
                \Mail::to($professional->user->email)
                    ->bcc('e.kadiyski@gmail.com')
                    ->queue(new SchoolVisitRequestRoleModel($schoolVisitsSelectedByTeacher));
            }
            
            return redirect('/visits')->with('msg_success', 'Успешно поканихте ролеви модел в класната си стая! Следваща стъпка е да се свържете с него/нея и да уговорите посещението.');
        } catch (Exception $e) {
            \DB::rollBack();
        }
    }
}